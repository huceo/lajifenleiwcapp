# 垃圾分类小程序

#### 介绍
通过调用天行数据垃圾分类接口实现的微信分类小程序，支持搜索框下拉联想、垃圾分类图像识别、语音识别和热搜榜功能。

垃圾分类文字版API接口：https://www.tianapi.com/apiview/97

垃圾分类图像版API接口：https://www.tianapi.com/apiview/101

垃圾分类语音版API接口：https://www.tianapi.com/apiview/116

垃圾分类搜索热点API接口：https://www.tianapi.com/apiview/107

导入到微信开发者工具后，在utils.js文件里，填上你在天行数据www.tianapi.com注册后获得的apikey即可使用。

#### 示例
![输入图片说明](https://images.gitee.com/uploads/images/2019/0715/122718_53df718f_113194.jpeg "微信图片_20190715111928.jpg")
